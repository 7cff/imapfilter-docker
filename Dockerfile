FROM alpine:3.8 as build

RUN apk add --no-cache git
RUN apk add --no-cache build-base
RUN apk add --no-cache lua5.1-dev
RUN apk add --no-cache openssl-dev
RUN apk add --no-cache pcre-dev
RUN git clone https://github.com/lefcha/imapfilter
WORKDIR /imapfilter
RUN git checkout tags/v2.6.16
RUN make all

FROM alpine:3.8

RUN apk add --no-cache ca-certificates
RUN apk add --no-cache lua5.1
RUN apk add --no-cache openssl
RUN apk add --no-cache pcre
COPY --from=build /imapfilter/src/imapfilter /usr/local/bin/imapfilter
COPY --from=build /imapfilter/src/account.lua /usr/local/share/imapfilter/account.lua
COPY --from=build /imapfilter/src/auxiliary.lua /usr/local/share/imapfilter/auxiliary.lua
COPY --from=build /imapfilter/src/common.lua /usr/local/share/imapfilter/common.lua
COPY --from=build /imapfilter/src/mailbox.lua /usr/local/share/imapfilter/mailbox.lua
COPY --from=build /imapfilter/src/message.lua /usr/local/share/imapfilter/message.lua
COPY --from=build /imapfilter/src/options.lua /usr/local/share/imapfilter/options.lua
COPY --from=build /imapfilter/src/regex.lua /usr/local/share/imapfilter/regex.lua
COPY --from=build /imapfilter/src/set.lua /usr/local/share/imapfilter/set.lua

CMD ["/usr/local/bin/imapfilter", "-c", "config.lua"]
